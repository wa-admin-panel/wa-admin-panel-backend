# python imports
import re

# django imports
from rest_framework import status
from rest_framework.test import APITestCase
from django.conf import settings
from django.core.management import call_command

# 3rd party
from voluptuous import Schema, Required

# project imports
from wa_api import urls
from core.models import Ban, CharacterTexture, MembershipWorld, Report, Room


class AdminApiTests(APITestCase):
    """Test that all functionality of the API works as expectated by
    WA-XCE commit 65c6ddfa9dc289fed8e67994282fb21e909a0ae1
    """

    def authenticate(self):
        self.client.credentials(HTTP_AUTHORIZATION=settings.ADMIN_API_TOKEN)

    def seed_data(self):
        """Fill db with test data
        The initial seed data should be the same as used for tests"""
        call_command("init-seed")

    def test_authentication(self):
        """A preshared secret must be configured and send in HTTP-Header"""
        # without an auth secret this should give 401
        for elem in urls.urlpatterns:
            if str(elem.pattern) == "openapi":
                continue
            response = self.client.get(f"/api/{str(elem.pattern)}")
            self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        # with a wrong auth secret this should give 401
        self.client.credentials(HTTP_AUTHORIZATION="this one is false")
        for elem in urls.urlpatterns:
            if str(elem.pattern) == "openapi":
                continue
            response = self.client.get(f"/api/{str(elem.pattern)}")
            self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        # but with the correct one it should never give 401
        self.authenticate()
        for elem in urls.urlpatterns:
            response = self.client.get(f"/api/{str(elem.pattern)}")
            self.assertNotEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_openapi_schema(self):
        """/api/openapi"""
        # the api schema should be exposed unauthenticated
        response = self.client.get("/api/openapi?format=openapi-json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue("openapi" in response.json())

    def test_ban_endpoint(self):
        """/api/ban (not verified jet with WA. Maybe unused)"""
        self.seed_data()
        self.authenticate()
        # test a valid request -> user is not baned
        room = Room.objects.get(name="Hallway")
        member = MembershipWorld.objects.get(player__username="funkypunky", world__name="Test World1")
        response = self.client.get(f"/api/ban?token={member.uuid_token}&roomUrl={room.url}")
        schema = Schema(
            {
                "is_banned": Required(bool),
                "message": Required(str),
            }
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        schema(response.json())
        self.assertFalse(response.json()["is_banned"])
        # test a valid request -> user is baned
        member = MembershipWorld.objects.get(player__username="queerglitter", world__name="Test World1")
        response = self.client.get(f"/api/ban?token={member.uuid_token}&roomUrl={room.url}")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(response.json()["is_banned"])
        self.assertEqual("You are evil!", response.json()["message"])
        # should still be banned if we ban the world all together
        ban = Ban.objects.get(membership_world__player__username="queerglitter")
        ban.room = None
        ban.save()
        response = self.client.get(f"/api/ban?token={member.uuid_token}&roomUrl={room.url}")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(response.json()["is_banned"])
        self.assertEqual("You are evil!", response.json()["message"])
        ban.room = room
        ban.save()
        # test nmissing params
        response = self.client.get("/api/ban?roomUrl={room.url}")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        # without room it should return ban status for whole world
        response = self.client.get(f"/api/ban?token={member.uuid_token}")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        schema(response.json())
        self.assertFalse(response.json()["is_banned"])
        response = self.client.get("/api/ban")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        # test wrong user
        response = self.client.get(f"/api/ban?token=idontexist&roomUrl={room.url}")
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        # test wrong room -> gets world ban status
        response = self.client.get(f"/api/ban?token={member.uuid_token}&roomUrl=idontexist")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        schema(response.json())
        self.assertFalse(response.json()["is_banned"])
        # test logic: inherit world bans for all rooms
        ban = Ban.objects.get(message="You are evil!")
        ban.room = None
        ban.save()
        response = self.client.get(f"/api/ban?token={member.uuid_token}")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        schema(response.json())
        self.assertTrue(response.json()["is_banned"])

    def test_map_endpoint(self):
        """/api/map
        userId parameter is unclear still
        """
        self.seed_data()
        self.authenticate()
        # test a valid request
        room = Room.objects.get(name="Hallway")
        response = self.client.get(f"/api/map?playUri={room.url}")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # schema of map object
        schema = Schema(
            {
                "mapUrl": Required(str),
                "policy_type": Required(int),
                "tags": Required([str]),
                "textures": Required([{"id": int, "level": int, "rights": str, "url": str}]),
                "roomSlug": str,
                "authenticationMandatory": bool,
                "contactPage": str,
                "group": str,
            }
        )
        schema(response.json())
        # test a non existing map
        response = self.client.get("/api/map?playUri=idontexist")
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        # test missing params
        response = self.client.get("/api/map")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        # test anonymous users get a map
        response = self.client.get(f"/api/map?playUri={room.url}&userId=uuid.uuid4()")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        schema(response.json())
        # test banned user get no map
        member = MembershipWorld.objects.get(player__username="queerglitter", world__name="Test World1")
        response = self.client.get(f"/api/map?playUri={room.url}&userId={member.uuid_token}")
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        # test banned user get no map (same with email)
        response = self.client.get(f"/api/map?playUri={room.url}&userId={member.player.email}")
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_report_endpoint(self):
        """/api/report
        (defunc with WA atm because of half filled report object)
        """
        self.seed_data()
        self.authenticate()
        # test a valid request
        room = Room.objects.get(name="Hallway")
        reporter_member = MembershipWorld.objects.get(player__username="funkypunky", world__name="Test World1")
        reported_member = MembershipWorld.objects.get(player__username="queerglitter", world__name="Test World1")
        report_data = {
            "reportWorldSlug": room.url,
            "reportedUserComment": "not nice",
            "reportedUserUuid": str(reported_member.uuid_token),
            "reporterUserUuid": str(reporter_member.uuid_token),
        }
        response = self.client.post("/api/report", data=report_data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertTrue(Report.objects.filter(user_comment=report_data["reportedUserComment"]).exists())
        # test empty comment not allowed
        report_data = {
            "reportWorldSlug": room.url,
            "reportedUserComment": "",
            "reportedUserUuid": str(reported_member.uuid_token),
            "reporterUserUuid": str(reporter_member.uuid_token),
        }
        response = self.client.post("/api/report", data=report_data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        # test wrong room
        report_data = {
            "reportWorldSlug": "idontexist",
            "reportedUserComment": "not nice",
            "reportedUserUuid": str(reported_member.uuid_token),
            "reporterUserUuid": str(reporter_member.uuid_token),
        }
        response = self.client.post("/api/report", data=report_data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        # test no reportedUser
        report_data = {
            "reportWorldSlug": room.url,
            "reportedUserComment": "not nice",
            "reportedUserUuid": "",
            "reporterUserUuid": str(reporter_member.uuid_token),
        }
        response = self.client.post("/api/report", data=report_data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_room_access_endpoint(self):
        """/api/room/access"""
        self.seed_data()
        self.authenticate()
        # test a valid request
        room = Room.objects.get(name="Hallway")
        member = MembershipWorld.objects.get(player__username="funkypunky", world__name="Test World1")
        response = self.client.get(f"/api/room/access?userIdentifier={member.uuid_token}&roomId={room.url}")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # schema of RoomAccess object
        schema = Schema(
            {
                "email": Required(str),
                "userUUID": Required(str),
                "tags": Required([str]),
                "textures": Required([{"id": int, "level": int, "rights": str, "url": str}]),
                "anonymous": Required(bool),
                "visitCardUrl": Required(str),
            }
        )
        schema(response.json())
        # should work also with email
        response = self.client.get(f"/api/room/access?userIdentifier={member.player.email}&roomId={room.url}")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        schema(response.json())
        # test a non member
        member = MembershipWorld.objects.get(player__username="funkypunky", world__name="Moon")
        response = self.client.get(f"/api/room/access?userIdentifier={member.uuid_token}&roomId={room.url}")
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        # test a non existing room
        response = self.client.get(f"/api/room/access?userIdentifier={member.uuid_token}&roomId=idontexist")
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        # test a non existing user
        response = self.client.get(f"/api/room/access?userIdentifier=idontexist&roomId={room.url}")
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        # test anonymous -> we get a valid access then
        response = self.client.get(f"/api/room/access?userIdentifier=&roomId={room.url}")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        schema(response.json())
        # test member by email -> valid access
        response = self.client.get(f"/api/room/access?userIdentifier={member.player.email}&roomId={room.url}")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        schema(response.json())
        # test missing params
        response = self.client.get(f"/api/room/access?roomId={room.url}")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        response = self.client.get(f"/api/room/access?userIdentifier={member.player.email}")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        response = self.client.get("/api/room/access")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        # banned user should get a 404
        member = MembershipWorld.objects.get(player__username="queerglitter", world__name="Test World1")
        response = self.client.get(f"/api/room/access?userIdentifier={member.uuid_token}&roomId={room.url}")
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        # banned user should get a 404 (also wit email address)
        member = MembershipWorld.objects.get(player__username="queerglitter", world__name="Test World1")
        response = self.client.get(f"/api/room/access?userIdentifier={member.player.email}&roomId={room.url}")
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_login_url_endpoint(self):
        """/api/login-url/<uuid>/"""
        self.seed_data()
        self.authenticate()
        # test a valid request
        member = MembershipWorld.objects.get(player__username="funkypunky", world__name="Test World1")
        # for now the uuid is our access token
        response = self.client.get(f"/api/login-url/{member.uuid_token}/")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # schema of admin access data object
        schema = Schema(
            {
                "roomUrl": str,
                "email": Required(str),
                "mapUrlStart": Required(str),
                "tags": Required([str]),
                "userUuid": Required(str),
                "textures": Required([{"id": int, "level": int, "rights": str, "url": str}]),
            }
        )
        schema(response.json())
        # test a non existing member
        response = self.client.get("/api/login-url/idontexist/")
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        # test there is no list view
        response = self.client.get("/api/login-url/")
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_room_sameworld_endpoint(self):
        """/api/room/sameWorld"""
        self.seed_data()
        self.authenticate()
        # test a valid request
        room = Room.objects.get(name="Hallway")
        response = self.client.get(f"/api/room/sameWorld?roomUrl={room.url}")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # schema of admin access data object
        schema = Schema([str])
        schema(response.json())
        # check that url includes protocol (needed for broadcast messages to work)
        url_re = r"^http.*$"
        self.assertTrue(re.match(url_re, response.json()[0]))
        # test a non existing room
        response = self.client.get("/api/room/sameWorld?roomUrl=idontexist")
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        # test no params
        response = self.client.get("/api/room/sameWorld")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_room_deactivated(self):
        self.seed_data()
        self.authenticate()
        # this room shows up in map endpoint
        room = Room.objects.get(name="Hallway")
        response = self.client.get(f"/api/map?playUri={room.url}")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # this room shows up in same world endpoint
        response = self.client.get(f"/api/room/sameWorld?roomUrl={room.url}")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(room.world.url + room.url in response.json())
        # but deactivated not any more
        room.is_deactivated = True
        room.save()
        response = self.client.get(f"/api/map?playUri={room.url}")
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        # also not in same world
        response = self.client.get(f"/api/room/sameWorld?roomUrl={room.url}")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertFalse(room.world.url + room.url in response.json())

    def test_character_texture_deactivated(self):
        self.seed_data()
        self.authenticate()
        # ct should be in room access list
        ct = CharacterTexture.objects.get(name="Texture1")
        room = Room.objects.get(name="Hallway")
        member = MembershipWorld.objects.get(player__username="funkypunky", world__name="Test World1")
        response = self.client.get(f"/api/room/access?userIdentifier={member.uuid_token}&roomId={room.url}")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        texture_id_list = [texture["id"] for texture in response.json()["textures"]]
        self.assertTrue(ct.pk in texture_id_list)
        # deactivated it should be out
        ct.is_deactivated = True
        ct.save()
        response = self.client.get(f"/api/room/access?userIdentifier={member.uuid_token}&roomId={room.url}")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        texture_id_list = [texture["id"] for texture in response.json()["textures"]]
        self.assertFalse(ct.pk in texture_id_list)
