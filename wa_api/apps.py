from django.apps import AppConfig


class WaApiConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "wa_api"
