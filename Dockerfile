# Use an official Python runtime based on Debian 11 "bullseye" as a parent image.
FROM python:3.9-bullseye AS base

# Add user that will be used in the container.
RUN useradd django

# Port used by this container to serve HTTP.
EXPOSE 8000

# Set environment variables.
# 1. Force Python stdout and stderr streams to be unbuffered.
# 2. Set PORT variable that is used by Gunicorn. This should match "EXPOSE"
#    command.
ENV PYTHONUNBUFFERED=1 \
    PORT=8000

# Install system packages required by Django in common use.
RUN apt-get update --yes --quiet && apt-get install --yes --quiet --no-install-recommends \
    build-essential \
    libpq-dev \
    default-libmysqlclient-dev \
    libjpeg62-turbo-dev \
    zlib1g-dev \
    libwebp-dev \
    libmagic-dev\
    gpg\
 && rm -rf /var/lib/apt/lists/*

# Install the project requirements.
COPY requirements.txt /
RUN pip install -r /requirements.txt

# Use /app folder as a directory where the source code is stored.
WORKDIR /app

# Set this directory to be owned by the "django" user. This Django project
# uses SQLite, the folder needs to be owned by the user that
# will be writing to the database file.
RUN chown django:django /app

# Copy the source code of the project into the container.
COPY --chown=django:django . .

FROM base AS production

# Install the project requirements.
COPY requirements_prod.txt /
RUN pip install -r /requirements_prod.txt

# Use user "django" to run the build commands below and the server itself.
USER django

# Collect static files.
RUN python manage.py collectstatic --noinput --clear

# Runtime command that executes when "docker run" is called, it does the
# following:
#   1. Migrate the database.
#   2. Start the application server.
# WARNING:
#   Migrating database at the same time as starting the server IS NOT THE BEST
#   PRACTICE. The database should be migrated manually or using the release
#   phase facilities of your hosting platform. This is used only so the
#   django instance can be started with a simple "docker run" command.
CMD set -xe; python manage.py migrate --noinput; gunicorn wa_backstage.wsgi_prod:application

FROM base AS develop

# Install the project dev requirements.
COPY requirements_dev.txt /
RUN pip install -r /requirements_dev.txt

WORKDIR /app

USER django

CMD set -xe; python manage.py migrate --noinput; python manage.py runserver 0.0.0.0:8000
