from django.urls import reverse, reverse_lazy
from core.models import MembershipWorld, Ban, Room
from frontend.forms import BanForm
from frontend.views import ReferralMixin
from unicorn_viewsets.mixins import DeleteModeMixin, UpdateModeMixin, CreateModeMixin
from unicorn_viewsets.views import GenericModeView


class BansView(ReferralMixin, CreateModeMixin, UpdateModeMixin, DeleteModeMixin, GenericModeView):
    model = Ban
    form_class = BanForm
    success_url = reverse_lazy("worlds")

    membership_world = None
    room = None

    rooms = []
    message = ""
    world = None

    is_world_banned = []

    message = ""

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def get_mode_kwargs(self):
        return {"world_pk": self.world.pk}

    def get_queryset(self):
        return self.model._default_manager.filter(membership_world__world=self.world).order_by("room")

    def get_success_url(self):
        return reverse("bans", kwargs=self.get_mode_kwargs())

    def get_form_data(self):
        data = super().get_form_data()
        data.update({"membership_world": self.membership_world, "room": self.room, "message": self.message})
        return data

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs["instance"].world = self.world
        return kwargs

    def set_attributes(self):
        super().set_attributes()

        if self.mode == "update":
            self.membership_world = self.object.membership_world
            self.world = self.membership_world.world
            self.room = self.object.room
            self.message = self.object.message
        if self.mode == "create":

            self.membership_world = MembershipWorld.objects.get(pk=self.request.resolver_match.kwargs["membership_pk"])
            self.world = self.membership_world.world  # get the corresponding world to the current membership
            self.rooms = Room.objects.filter(world=self.world).exclude(
                policy_type=1
            )  # Exclude rooms with unrestricted access

            self.message = ""
            self.is_world_banned = Ban.objects.filter(membership_world=self.membership_world, room=None)
