# Routers

> Auto-generated documentation for [unicorn_viewsets.routers](blob/master/unicorn_viewsets/routers.py) module.

- [Backstage](../README.md#wa-backstage) / [Modules](../MODULES.md#backstage-modules) / [Unicorn Viewsets](index.md#unicorn-viewsets) / Routers
    - [Router](#router)
        - [Router().get_routes](#routerget_routes)
        - [Router().register](#routerregister)
        - [Router().urls](#routerurls)

## Router

[[find in source code]](blob/master/unicorn_viewsets/routers.py#L4)

```python
class Router():
    def __init__(view_class, root_url_name=None):
```

### Router().get_routes

[[find in source code]](blob/master/unicorn_viewsets/routers.py#L10)

```python
def get_routes():
```

### Router().register

[[find in source code]](blob/master/unicorn_viewsets/routers.py#L20)

```python
def register(prefix, component_name):
```

### Router().urls

[[find in source code]](blob/master/unicorn_viewsets/routers.py#L23)

```python
@property
def urls():
```
