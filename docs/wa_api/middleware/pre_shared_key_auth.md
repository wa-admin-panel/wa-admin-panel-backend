# Pre Shared Key Auth

> Auto-generated documentation for [wa_api.middleware.pre_shared_key_auth](blob/master/wa_api/middleware/pre_shared_key_auth.py) module.

- [Backstage](../../README.md#wa-backstage) / [Modules](../../MODULES.md#backstage-modules) / [Wa Api](../index.md#wa-api) / [Middleware](index.md#middleware) / Pre Shared Key Auth
    - [PreSharedKeyAuthMiddleware](#presharedkeyauthmiddleware)
        - [PreSharedKeyAuthMiddleware().process_view](#presharedkeyauthmiddlewareprocess_view)

## PreSharedKeyAuthMiddleware

[[find in source code]](blob/master/wa_api/middleware/pre_shared_key_auth.py#L6)

```python
class PreSharedKeyAuthMiddleware():
    def __init__(get_response):
```

This middleware checks if a pre shared secret is send as Authorization-Header
It is applied to all paths under the /api/ endpoint
Default: if not defined in patterns this middleware is not applied
URL_ALLOW_PATTERNS override disallow patterns

### PreSharedKeyAuthMiddleware().process_view

[[find in source code]](blob/master/wa_api/middleware/pre_shared_key_auth.py#L52)

```python
def process_view(request, view_func, view_args, view_kwargs):
```
