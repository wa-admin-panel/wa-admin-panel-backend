# Tests

> Auto-generated documentation for [wa_api.tests](blob/master/wa_api/tests.py) module.

- [Backstage](../README.md#wa-backstage) / [Modules](../MODULES.md#backstage-modules) / [Wa Api](index.md#wa-api) / Tests
    - [AdminApiTests](#adminapitests)
        - [AdminApiTests().authenticate](#adminapitestsauthenticate)
        - [AdminApiTests().seed_data](#adminapitestsseed_data)
        - [AdminApiTests().test_authentication](#adminapiteststest_authentication)
        - [AdminApiTests().test_ban_endpoint](#adminapiteststest_ban_endpoint)
        - [AdminApiTests().test_character_texture_deactivated](#adminapiteststest_character_texture_deactivated)
        - [AdminApiTests().test_login_url_endpoint](#adminapiteststest_login_url_endpoint)
        - [AdminApiTests().test_map_endpoint](#adminapiteststest_map_endpoint)
        - [AdminApiTests().test_openapi_schema](#adminapiteststest_openapi_schema)
        - [AdminApiTests().test_report_endpoint](#adminapiteststest_report_endpoint)
        - [AdminApiTests().test_room_access_endpoint](#adminapiteststest_room_access_endpoint)
        - [AdminApiTests().test_room_deactivated](#adminapiteststest_room_deactivated)
        - [AdminApiTests().test_room_sameworld_endpoint](#adminapiteststest_room_sameworld_endpoint)

## AdminApiTests

[[find in source code]](blob/master/wa_api/tests.py#L18)

```python
class AdminApiTests(APITestCase):
```

Test that all functionality of the API works as expectated by
WA-XCE commit 65c6ddfa9dc289fed8e67994282fb21e909a0ae1

### AdminApiTests().authenticate

[[find in source code]](blob/master/wa_api/tests.py#L23)

```python
def authenticate():
```

### AdminApiTests().seed_data

[[find in source code]](blob/master/wa_api/tests.py#L26)

```python
def seed_data():
```

Fill db with test data
The initial seed data should be the same as used for tests

### AdminApiTests().test_authentication

[[find in source code]](blob/master/wa_api/tests.py#L31)

```python
def test_authentication():
```

A preshared secret must be configured and send in HTTP-Header

### AdminApiTests().test_ban_endpoint

[[find in source code]](blob/master/wa_api/tests.py#L59)

```python
def test_ban_endpoint():
```

/api/ban (not verified jet with WA. Maybe unused)

### AdminApiTests().test_character_texture_deactivated

[[find in source code]](blob/master/wa_api/tests.py#L337)

```python
def test_character_texture_deactivated():
```

### AdminApiTests().test_login_url_endpoint

[[find in source code]](blob/master/wa_api/tests.py#L267)

```python
def test_login_url_endpoint():
```

/api/login-url/<uuid>/

### AdminApiTests().test_map_endpoint

[[find in source code]](blob/master/wa_api/tests.py#L119)

```python
def test_map_endpoint():
```

/api/map
userId parameter is unclear still

### AdminApiTests().test_openapi_schema

[[find in source code]](blob/master/wa_api/tests.py#L52)

```python
def test_openapi_schema():
```

/api/openapi

### AdminApiTests().test_report_endpoint

[[find in source code]](blob/master/wa_api/tests.py#L161)

```python
def test_report_endpoint():
```

/api/report
(defunc with WA atm because of half filled report object)

### AdminApiTests().test_room_access_endpoint

[[find in source code]](blob/master/wa_api/tests.py#L208)

```python
def test_room_access_endpoint():
```

/api/room/access

### AdminApiTests().test_room_deactivated

[[find in source code]](blob/master/wa_api/tests.py#L316)

```python
def test_room_deactivated():
```

### AdminApiTests().test_room_sameworld_endpoint

[[find in source code]](blob/master/wa_api/tests.py#L295)

```python
def test_room_sameworld_endpoint():
```

/api/room/sameWorld
