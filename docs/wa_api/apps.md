# Apps

> Auto-generated documentation for [wa_api.apps](blob/master/wa_api/apps.py) module.

- [Backstage](../README.md#wa-backstage) / [Modules](../MODULES.md#backstage-modules) / [Wa Api](index.md#wa-api) / Apps
    - [WaApiConfig](#waapiconfig)

## WaApiConfig

[[find in source code]](blob/master/wa_api/apps.py#L4)

```python
class WaApiConfig(AppConfig):
```
