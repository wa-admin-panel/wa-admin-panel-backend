# Migrations

> Auto-generated documentation for [core.migrations](blob/master/core/migrations/__init__.py) module.

- [Backstage](../../README.md#wa-backstage) / [Modules](../../MODULES.md#backstage-modules) / [Core](../index.md#core) / Migrations
    - Modules
        - [0001 User Model](0001_user_model.md#0001-user-model)
        - [0002 Initial Models](0002_initial_models.md#0002-initial-models)
        - [0003 Auto 20220127 1058](0003_auto_20220127_1058.md#0003-auto-20220127-1058)
        - [0004 Alter Membershipworld Unique Together](0004_alter_membershipworld_unique_together.md#0004-alter-membershipworld-unique-together)
        - [0005 Alter Room World](0005_alter_room_world.md#0005-alter-room-world)
        - [0006 Alter Membershipworld Player](0006_alter_membershipworld_player.md#0006-alter-membershipworld-player)
        - [0007 World Slug](0007_world_slug.md#0007-world-slug)
        - [0008 Membershipworld Anonymous](0008_membershipworld_anonymous.md#0008-membershipworld-anonymous)
        - [0009 Alter Room Unique Together](0009_alter_room_unique_together.md#0009-alter-room-unique-together)
        - [0010 Auto 20220207 1439](0010_auto_20220207_1439.md#0010-auto-20220207-1439)
        - [0011 Auto 20220207 1628](0011_auto_20220207_1628.md#0011-auto-20220207-1628)
        - [0012 Room Is Deactivated](0012_room_is_deactivated.md#0012-room-is-deactivated)
        - [0013 Report Relate To Room](0013_report_relate_to_room.md#0013-report-relate-to-room)
        - [0014 Auto 20220309 1334](0014_auto_20220309_1334.md#0014-auto-20220309-1334)
        - [0015 Alter Membershipworld World](0015_alter_membershipworld_world.md#0015-alter-membershipworld-world)
        - [0016 Auto 20220401 1305](0016_auto_20220401_1305.md#0016-auto-20220401-1305)
        - [0017 Auto 20220427 0934](0017_auto_20220427_0934.md#0017-auto-20220427-0934)
        - [0018 Alter Room Map Url](0018_alter_room_map_url.md#0018-alter-room-map-url)
