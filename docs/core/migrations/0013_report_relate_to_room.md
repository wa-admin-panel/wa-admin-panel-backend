# 0013 Report Relate To Room

> Auto-generated documentation for [core.migrations.0013_report_relate_to_room](blob/master/core/migrations/0013_report_relate_to_room.py) module.

- [Backstage](../../README.md#wa-backstage) / [Modules](../../MODULES.md#backstage-modules) / [Core](../index.md#core) / [Migrations](index.md#migrations) / 0013 Report Relate To Room
    - [Migration](#migration)
    - [all_reports_to_first_room](#all_reports_to_first_room)
    - [do_nothing](#do_nothing)

## Migration

[[find in source code]](blob/master/core/migrations/0013_report_relate_to_room.py#L22)

```python
class Migration(migrations.Migration):
```

## all_reports_to_first_room

[[find in source code]](blob/master/core/migrations/0013_report_relate_to_room.py#L7)

```python
def all_reports_to_first_room(apps, schema_editor):
```

## do_nothing

[[find in source code]](blob/master/core/migrations/0013_report_relate_to_room.py#L18)

```python
def do_nothing(apps, schema_editor):
```
