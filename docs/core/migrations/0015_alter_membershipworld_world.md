# 0015 Alter Membershipworld World

> Auto-generated documentation for [core.migrations.0015_alter_membershipworld_world](blob/master/core/migrations/0015_alter_membershipworld_world.py) module.

- [Backstage](../../README.md#wa-backstage) / [Modules](../../MODULES.md#backstage-modules) / [Core](../index.md#core) / [Migrations](index.md#migrations) / 0015 Alter Membershipworld World
    - [Migration](#migration)

## Migration

[[find in source code]](blob/master/core/migrations/0015_alter_membershipworld_world.py#L7)

```python
class Migration(migrations.Migration):
```
