# Init-seed

> Auto-generated documentation for [core.management.commands.init-seed](blob/master/core/management/commands/init-seed.py) module.

- [Backstage](../../../README.md#wa-backstage) / [Modules](../../../MODULES.md#backstage-modules) / [Core](../../index.md#core) / [Management](../index.md#management) / [Commands](index.md#commands) / Init-seed
    - [Command](#command)
        - [Command().handle](#commandhandle)
    - [TestData](#testdata)
        - [TestData().aggregate_objects](#testdataaggregate_objects)

## Command

[[find in source code]](blob/master/core/management/commands/init-seed.py#L116)

```python
class Command(BaseCommand):
```

This django manage.py command 'init-seed' seeds some data to the database
for testing purposes

If you want to delete the seeded test data, run:
python manage.py flush

### Command().handle

[[find in source code]](blob/master/core/management/commands/init-seed.py#L133)

```python
def handle(*args, **options):
```

## TestData

[[find in source code]](blob/master/core/management/commands/init-seed.py#L6)

```python
class TestData():
    def __init__():
```

This class contains the lists of test data

### TestData().aggregate_objects

[[find in source code]](blob/master/core/management/commands/init-seed.py#L98)

```python
def aggregate_objects():
```

The aggregate_objects function is used to export an overall list of all
test objects which are to be written to the database
