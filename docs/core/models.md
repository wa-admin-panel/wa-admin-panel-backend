# Models

> Auto-generated documentation for [core.models](blob/master/core/models.py) module.

- [Backstage](../README.md#wa-backstage) / [Modules](../MODULES.md#backstage-modules) / [Core](index.md#core) / Models
    - [AdminTag](#admintag)
        - [AdminTag().save](#admintagsave)
    - [BackstageUser](#backstageuser)
    - [Ban](#ban)
    - [CharacterTexture](#charactertexture)
    - [MembershipWorld](#membershipworld)
        - [MembershipWorld().get_world_access_url](#membershipworldget_world_access_url)
        - [MembershipWorld().is_admin](#membershipworldis_admin)
    - [MembershipWorldQuerySet](#membershipworldqueryset)
        - [MembershipWorldQuerySet().exclude_anonymous](#membershipworldquerysetexclude_anonymous)
    - [Player](#player)
    - [PolicyType](#policytype)
    - [Report](#report)
    - [Room](#room)
        - [Room().contact_page](#roomcontact_page)
    - [Tag](#tag)
    - [World](#world)

## AdminTag

[[find in source code]](blob/master/core/models.py#L40)

```python
class AdminTag(SingletonModel, Tag):
```

#### See also

- [SingletonModel](utils.md#singletonmodel)
- [Tag](#tag)

### AdminTag().save

[[find in source code]](blob/master/core/models.py#L51)

```python
def save(*args, **kwargs):
```

## BackstageUser

[[find in source code]](blob/master/core/models.py#L23)

```python
class BackstageUser(AbstractUser):
```

## Ban

[[find in source code]](blob/master/core/models.py#L199)

```python
class Ban(models.Model):
```

#### Attributes

- `constraints` - unique_together = [["membership_world", "room"]]: `[models.UniqueConstraint(fields=['membership_wo...`

## CharacterTexture

[[find in source code]](blob/master/core/models.py#L56)

```python
class CharacterTexture(models.Model):
```

#### Attributes

- `policy_type` - rights implemented in WA without any functionality 02/22
  maybe some tagging feature was planed once
  not implemented in WA Code
  a wa-backstage implementation is provided: `models.IntegerField(verbose_name=_('Policy Type...`

## MembershipWorld

[[find in source code]](blob/master/core/models.py#L170)

```python
class MembershipWorld(models.Model):
```

### MembershipWorld().get_world_access_url

[[find in source code]](blob/master/core/models.py#L192)

```python
def get_world_access_url():
```

### MembershipWorld().is_admin

[[find in source code]](blob/master/core/models.py#L195)

```python
def is_admin():
```

## MembershipWorldQuerySet

[[find in source code]](blob/master/core/models.py#L165)

```python
class MembershipWorldQuerySet(models.QuerySet):
```

### MembershipWorldQuerySet().exclude_anonymous

[[find in source code]](blob/master/core/models.py#L166)

```python
def exclude_anonymous():
```

## Player

[[find in source code]](blob/master/core/models.py#L151)

```python
class Player(models.Model):
```

## PolicyType

[[find in source code]](blob/master/core/models.py#L17)

```python
class PolicyType(models.IntegerChoices):
```

## Report

[[find in source code]](blob/master/core/models.py#L231)

```python
class Report(models.Model):
```

## Room

[[find in source code]](blob/master/core/models.py#L105)

```python
class Room(models.Model):
```

### Room().contact_page

[[find in source code]](blob/master/core/models.py#L132)

```python
@property
def contact_page():
```

We use contact page URL of world if not specified on room

## Tag

[[find in source code]](blob/master/core/models.py#L27)

```python
class Tag(models.Model):
```

## World

[[find in source code]](blob/master/core/models.py#L80)

```python
class World(models.Model):
```

#### Attributes

- `use_oauth` - oauth provider is configured per WA instance env
  this is automatically turned off for policy_type 1: `models.BooleanField(verbose_name=_('Redirect to...`
