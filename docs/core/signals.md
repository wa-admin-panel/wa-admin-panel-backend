# Signals

> Auto-generated documentation for [core.signals](blob/master/core/signals.py) module.

- [Backstage](../README.md#wa-backstage) / [Modules](../MODULES.md#backstage-modules) / [Core](index.md#core) / Signals
    - [assign_matching_character_textures_to_member](#assign_matching_character_textures_to_member)
    - [assign_matching_character_textures_to_members](#assign_matching_character_textures_to_members)
    - [create_anonymous_membership](#create_anonymous_membership)
    - [only_one_entry_room](#only_one_entry_room)
    - [send_backstage_user_activation_mail](#send_backstage_user_activation_mail)
    - [set_world_slug](#set_world_slug)

## assign_matching_character_textures_to_member

[[find in source code]](blob/master/core/signals.py#L108)

```python
@receiver(m2m_changed, sender=MembershipWorld.tags.through)
def assign_matching_character_textures_to_member(sender, instance, **kwargs):
```

## assign_matching_character_textures_to_members

[[find in source code]](blob/master/core/signals.py#L77)

```python
@receiver(m2m_changed, sender=CharacterTexture.tags.through)
@receiver(m2m_changed, sender=CharacterTexture.world_set.through)
@receiver(post_save, sender=CharacterTexture)
def assign_matching_character_textures_to_members(sender, instance, **kwargs):
```

CTs are assigned to members with a logic of policy_type and tags
We save and update these assignments in the db for performance reasons
When a CT is created/updated, it is automatically assigned/reassigned to members

## create_anonymous_membership

[[find in source code]](blob/master/core/signals.py#L54)

```python
@receiver(post_save, sender=World)
def create_anonymous_membership(sender, instance, created, **kwargs):
```

together with each new world we create the membership of an anonymous user

## only_one_entry_room

[[find in source code]](blob/master/core/signals.py#L65)

```python
@receiver(post_save, sender=Room)
def only_one_entry_room(sender, instance, **kwargs):
```

if room is entry_room unset all other entry_rooms

## send_backstage_user_activation_mail

[[find in source code]](blob/master/core/signals.py#L18)

```python
@receiver(post_save, sender=BackstageUser)
def send_backstage_user_activation_mail(sender, instance, created, **kwargs):
```

Send an email for password setting to a newly created backstage user.

## set_world_slug

[[find in source code]](blob/master/core/signals.py#L46)

```python
@receiver(pre_save, sender=World)
def set_world_slug(sender, instance, update_fields=None, **kwargs):
```

receiver to auto set slug on world
