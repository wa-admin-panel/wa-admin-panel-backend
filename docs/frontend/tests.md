# Tests

> Auto-generated documentation for [frontend.tests](blob/master/frontend/tests.py) module.

- [Backstage](../README.md#wa-backstage) / [Modules](../MODULES.md#backstage-modules) / [Frontend](index.md#frontend) / Tests
    - [AuthenticatedFrontendTestCase](#authenticatedfrontendtestcase)
        - [AuthenticatedFrontendTestCase().setUp](#authenticatedfrontendtestcasesetup)
        - [AuthenticatedFrontendTestCase().test_players_page_renders_successfully](#authenticatedfrontendtestcasetest_players_page_renders_successfully)
        - [AuthenticatedFrontendTestCase().test_users_page_renders_successfully](#authenticatedfrontendtestcasetest_users_page_renders_successfully)
        - [AuthenticatedFrontendTestCase().test_worlds_page_renders_successfully](#authenticatedfrontendtestcasetest_worlds_page_renders_successfully)

## AuthenticatedFrontendTestCase

[[find in source code]](blob/master/frontend/tests.py#L7)

```python
class AuthenticatedFrontendTestCase(TestCase):
```

### AuthenticatedFrontendTestCase().setUp

[[find in source code]](blob/master/frontend/tests.py#L8)

```python
def setUp():
```

### AuthenticatedFrontendTestCase().test_players_page_renders_successfully

[[find in source code]](blob/master/frontend/tests.py#L12)

```python
def test_players_page_renders_successfully():
```

Player list should render without errors.

### AuthenticatedFrontendTestCase().test_users_page_renders_successfully

[[find in source code]](blob/master/frontend/tests.py#L18)

```python
def test_users_page_renders_successfully():
```

User list should render without errors.

Regression test for #118.

### AuthenticatedFrontendTestCase().test_worlds_page_renders_successfully

[[find in source code]](blob/master/frontend/tests.py#L27)

```python
def test_worlds_page_renders_successfully():
```

World list should render without errors.
