# Profile

> Auto-generated documentation for [frontend.components.profile](blob/master/frontend/components/profile.py) module.

- [Backstage](../../README.md#wa-backstage) / [Modules](../../MODULES.md#backstage-modules) / [Frontend](../index.md#frontend) / [Components](index.md#components) / Profile
    - [ProfileView](#profileview)
        - [ProfileView().get_context_data](#profileviewget_context_data)
        - [ProfileView().get_form_kwargs](#profileviewget_form_kwargs)
        - [ProfileView().get_success_url](#profileviewget_success_url)
        - [ProfileView().redirect_to_success_url](#profileviewredirect_to_success_url)
        - [ProfileView().validate_and_update](#profileviewvalidate_and_update)

## ProfileView

[[find in source code]](blob/master/frontend/components/profile.py#L11)

```python
class ProfileView(UnicornView):
```

### ProfileView().get_context_data

[[find in source code]](blob/master/frontend/components/profile.py#L31)

```python
def get_context_data(**kwargs):
```

### ProfileView().get_form_kwargs

[[find in source code]](blob/master/frontend/components/profile.py#L36)

```python
def get_form_kwargs():
```

### ProfileView().get_success_url

[[find in source code]](blob/master/frontend/components/profile.py#L39)

```python
def get_success_url():
```

### ProfileView().redirect_to_success_url

[[find in source code]](blob/master/frontend/components/profile.py#L20)

```python
def redirect_to_success_url():
```

### ProfileView().validate_and_update

[[find in source code]](blob/master/frontend/components/profile.py#L23)

```python
def validate_and_update():
```
