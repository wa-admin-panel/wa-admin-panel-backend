# Rooms

> Auto-generated documentation for [frontend.components.rooms](blob/master/frontend/components/rooms.py) module.

- [Backstage](../../README.md#wa-backstage) / [Modules](../../MODULES.md#backstage-modules) / [Frontend](../index.md#frontend) / [Components](index.md#components) / Rooms
    - [RoomsView](#roomsview)
        - [RoomsView().get_form_data](#roomsviewget_form_data)
        - [RoomsView().get_form_kwargs](#roomsviewget_form_kwargs)
        - [RoomsView().get_mode_kwargs](#roomsviewget_mode_kwargs)
        - [RoomsView().get_queryset](#roomsviewget_queryset)
        - [RoomsView().get_success_url](#roomsviewget_success_url)
        - [RoomsView().set_attributes](#roomsviewset_attributes)

## RoomsView

[[find in source code]](blob/master/frontend/components/rooms.py#L11)

```python
FilterSelectWidgetDecorator(
    queryset=Tag.objects.exclude(name='admin'),
    form_field='tags',
)
class RoomsView(
    CreateModeMixin,
    DeleteModeMixin,
    UpdateModeMixin,
    GenericModeView,
):
    def __init__(**kwargs):
```

#### Attributes

- `policy_type_choices` - lazy loading whatever bullshit with choices class: `[(choice[0], str(choice[1])) for choice in PolicyType.choices]`

#### See also

- [CreateModeMixin](../../unicorn_viewsets/mixins.md#createmodemixin)
- [DeleteModeMixin](../../unicorn_viewsets/mixins.md#deletemodemixin)
- [GenericModeView](../../unicorn_viewsets/views.md#genericmodeview)
- [UpdateModeMixin](../../unicorn_viewsets/mixins.md#updatemodemixin)

### RoomsView().get_form_data

[[find in source code]](blob/master/frontend/components/rooms.py#L42)

```python
def get_form_data():
```

### RoomsView().get_form_kwargs

[[find in source code]](blob/master/frontend/components/rooms.py#L57)

```python
def get_form_kwargs():
```

### RoomsView().get_mode_kwargs

[[find in source code]](blob/master/frontend/components/rooms.py#L62)

```python
def get_mode_kwargs():
```

### RoomsView().get_queryset

[[find in source code]](blob/master/frontend/components/rooms.py#L65)

```python
def get_queryset():
```

### RoomsView().get_success_url

[[find in source code]](blob/master/frontend/components/rooms.py#L68)

```python
def get_success_url():
```

### RoomsView().set_attributes

[[find in source code]](blob/master/frontend/components/rooms.py#L71)

```python
def set_attributes():
```
