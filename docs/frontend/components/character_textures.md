# Character Textures

> Auto-generated documentation for [frontend.components.character_textures](blob/master/frontend/components/character_textures.py) module.

- [Backstage](../../README.md#wa-backstage) / [Modules](../../MODULES.md#backstage-modules) / [Frontend](../index.md#frontend) / [Components](index.md#components) / Character Textures
    - [CharacterTexturesView](#charactertexturesview)
        - [CharacterTexturesView().filtered_list](#charactertexturesviewfiltered_list)
        - [CharacterTexturesView().get_form_data](#charactertexturesviewget_form_data)
        - [CharacterTexturesView().get_mode_kwargs](#charactertexturesviewget_mode_kwargs)
        - [CharacterTexturesView().prepopulate_fields](#charactertexturesviewprepopulate_fields)
        - [CharacterTexturesView().set_attributes](#charactertexturesviewset_attributes)

## CharacterTexturesView

[[find in source code]](blob/master/frontend/components/character_textures.py#L12)

```python
FilterSelectWidgetDecorator(
    queryset=Tag.objects.exclude(name='admin'),
    form_field='tags',
)
FilterSelectWidgetDecorator(queryset=World.objects.all(), form_field='worlds')
class CharacterTexturesView(
    CreateModeMixin,
    DeleteModeMixin,
    UpdateModeMixin,
    GenericModeView,
    GenericModeView.Meta,
):
```

#### Attributes

- `policy_type_choices` - lazy loading whatever bullshit with choices class: `[(choice[0], str(choice[1])) for choice in PolicyType.choices]`
- `filter_choices` - unicorn somewhere converts numbers to string on the way. So all strings...: `[['0', _('all')]] + [[str(world.pk), world.name...`

#### See also

- [CreateModeMixin](../../unicorn_viewsets/mixins.md#createmodemixin)
- [DeleteModeMixin](../../unicorn_viewsets/mixins.md#deletemodemixin)
- [GenericModeView](../../unicorn_viewsets/views.md#genericmodeview)
- [UpdateModeMixin](../../unicorn_viewsets/mixins.md#updatemodemixin)

### CharacterTexturesView().filtered_list

[[find in source code]](blob/master/frontend/components/character_textures.py#L32)

```python
def filtered_list():
```

### CharacterTexturesView().get_form_data

[[find in source code]](blob/master/frontend/components/character_textures.py#L41)

```python
def get_form_data():
```

### CharacterTexturesView().get_mode_kwargs

[[find in source code]](blob/master/frontend/components/character_textures.py#L60)

```python
def get_mode_kwargs():
```

### CharacterTexturesView().prepopulate_fields

[[find in source code]](blob/master/frontend/components/character_textures.py#L66)

```python
def prepopulate_fields(form):
```

### CharacterTexturesView().set_attributes

[[find in source code]](blob/master/frontend/components/character_textures.py#L51)

```python
def set_attributes():
```
