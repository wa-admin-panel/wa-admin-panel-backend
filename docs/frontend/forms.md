# Forms

> Auto-generated documentation for [frontend.forms](blob/master/frontend/forms.py) module.

- [Backstage](../README.md#wa-backstage) / [Modules](../MODULES.md#backstage-modules) / [Frontend](index.md#frontend) / Forms
    - [BackstageUserForm](#backstageuserform)
        - [BackstageUserForm().clean_email](#backstageuserformclean_email)
        - [BackstageUserForm().clean_username](#backstageuserformclean_username)
    - [BanForm](#banform)
        - [BanForm().save](#banformsave)
    - [CharacterTextureForm](#charactertextureform)
    - [ImportForm](#importform)
        - [ImportForm().clean_email](#importformclean_email)
        - [ImportForm().clean_file](#importformclean_file)
        - [ImportForm().clean_username](#importformclean_username)
    - [MembershipWorldForm](#membershipworldform)
    - [PlayerForm](#playerform)
    - [ProfileForm](#profileform)
        - [ProfileForm().clean](#profileformclean)
        - [ProfileForm().clean_new_password1](#profileformclean_new_password1)
        - [ProfileForm().clean_old_password](#profileformclean_old_password)
        - [ProfileForm().save](#profileformsave)
    - [RoomForm](#roomform)
    - [TagForm](#tagform)
    - [WorldForm](#worldform)

## BackstageUserForm

[[find in source code]](blob/master/frontend/forms.py#L14)

```python
class BackstageUserForm(forms.ModelForm):
```

### BackstageUserForm().clean_email

[[find in source code]](blob/master/frontend/forms.py#L19)

```python
def clean_email():
```

### BackstageUserForm().clean_username

[[find in source code]](blob/master/frontend/forms.py#L22)

```python
def clean_username():
```

## BanForm

[[find in source code]](blob/master/frontend/forms.py#L89)

```python
class BanForm(forms.ModelForm):
```

### BanForm().save

[[find in source code]](blob/master/frontend/forms.py#L94)

```python
def save(commit=True):
```

## CharacterTextureForm

[[find in source code]](blob/master/frontend/forms.py#L146)

```python
class CharacterTextureForm(forms.ModelForm):
```

## ImportForm

[[find in source code]](blob/master/frontend/forms.py#L38)

```python
class ImportForm(forms.Form):
```

### ImportForm().clean_email

[[find in source code]](blob/master/frontend/forms.py#L51)

```python
def clean_email():
```

### ImportForm().clean_file

[[find in source code]](blob/master/frontend/forms.py#L41)

```python
def clean_file():
```

### ImportForm().clean_username

[[find in source code]](blob/master/frontend/forms.py#L54)

```python
def clean_username():
```

## MembershipWorldForm

[[find in source code]](blob/master/frontend/forms.py#L81)

```python
class MembershipWorldForm(forms.ModelForm):
```

## PlayerForm

[[find in source code]](blob/master/frontend/forms.py#L32)

```python
class PlayerForm(forms.ModelForm):
```

## ProfileForm

[[find in source code]](blob/master/frontend/forms.py#L101)

```python
class ProfileForm(forms.ModelForm):
```

### ProfileForm().clean

[[find in source code]](blob/master/frontend/forms.py#L122)

```python
def clean():
```

### ProfileForm().clean_new_password1

[[find in source code]](blob/master/frontend/forms.py#L116)

```python
def clean_new_password1():
```

### ProfileForm().clean_old_password

[[find in source code]](blob/master/frontend/forms.py#L110)

```python
def clean_old_password():
```

### ProfileForm().save

[[find in source code]](blob/master/frontend/forms.py#L133)

```python
def save(commit=True):
```

## RoomForm

[[find in source code]](blob/master/frontend/forms.py#L58)

```python
class RoomForm(forms.ModelForm):
```

## TagForm

[[find in source code]](blob/master/frontend/forms.py#L140)

```python
class TagForm(forms.ModelForm):
```

## WorldForm

[[find in source code]](blob/master/frontend/forms.py#L26)

```python
class WorldForm(forms.ModelForm):
```
