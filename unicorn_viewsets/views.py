from django.core.exceptions import ImproperlyConfigured, ObjectDoesNotExist
from django.shortcuts import redirect
from django.urls import reverse
from django.views.generic import TemplateView
from django_unicorn.components import LocationUpdate, UnicornView


class ComponentView(TemplateView):
    component_name = ""

    def get_context_data(self, **kwargs):
        return super().get_context_data(component_name=self.component_name, **kwargs)


class GenericModeView(UnicornView):
    form_class = None
    mode = None
    mode_template_name = ""
    _model = None
    queryset = None
    object = None
    object_list = []
    pk = None
    success_url = None
    template_name = "unicorn_viewsets/mode-view.html"

    class Meta:
        exclude = ["form_class", "model", "queryset", "success_url"]
        javascript_exclude = ["object"]

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.mode = self.get_mode()
        self.pk = self.request.resolver_match.kwargs.get("pk")
        self.set_attributes()

    @property
    def model(self):
        if self._model:
            return self._model
        else:
            raise ImproperlyConfigured("This unicorn model viewset needs a model attribute configured.")

    @model.setter
    def model(self, value):
        self._model = value

    def get_errors(self, form):
        return form.errors.get_json_data(escape_html=True)

    def get_form(self):
        return self.get_form_class()(**self.get_form_kwargs())

    def get_form_class(self):
        return self.form_class

    def get_form_data(self):
        return {}

    def get_form_kwargs(self):
        return {
            "data": self.get_form_data(),
            "instance": self.get_object(),
        }

    def get_mode(self):
        mode = None
        url_name: str = self.request.resolver_match.url_name
        if url_name.startswith(self.component_name) and (len(url_name) > len(self.component_name)):
            _, _, mode = url_name.rpartition("-")
        return mode

    def get_mode_kwargs(self):
        return {}

    def get_object(self):
        try:
            return self.get_queryset().get(pk=self.pk)
        except ObjectDoesNotExist:
            return self.model()

    def get_queryset(self):
        if self.queryset is not None:
            return self.queryset.all()
        else:
            return self.model._default_manager.all()

    def get_success_url(self):
        return self.success_url

    def needs_page_reload(self):
        # All default actions just change the component mode, so there is no page reload needed for redirecting to the
        # success URL. Maybe overridden for component external redirects.
        return False

    def set_attributes(self):
        self.object = self.get_object()
        self.object_list = self.get_queryset()
        if self.mode:
            self.mode_template_name = f"unicorn/{self.component_name}-{self.mode}.html"
        else:
            self.mode_template_name = f"unicorn/{self.component_name}.html"

    def switch_mode(self, mode=None, **kwargs):
        self.mode = mode
        self.pk = kwargs.get("pk")
        self.set_attributes()
        if mode:
            mode_kwargs = self.get_mode_kwargs()
            mode_kwargs.update(kwargs)
            url = reverse(f"{self.component_name}-{mode}", kwargs=mode_kwargs)
        else:
            url = self.get_success_url()
        response = redirect(url)
        if not self.needs_page_reload():
            response = LocationUpdate(response)
        return response

    def prepopulate_fields(self, form):
        """override to pass values into form fields automatically
        Edit form.data['my_auto_field']
        """
        ...

    def validate_and_save_form(self):
        form = self.get_form()
        self.prepopulate_fields(form)
        is_valid = form.is_valid()
        # self.errors is used by Unicorn to propagate validation errors to the template.
        # https://www.django-unicorn.com/docs/validation/
        self.errors = self.get_errors(form)
        if is_valid:
            form.save()
            return self.switch_mode()

    def redirect_to_success_url(self):
        return self.switch_mode()

    def calling(self, name, args):
        # We reset self.errors before doing our own form validation. As Unicorn uses cached components, self.errors
        # might be set when actions are called (because of a previous validation). See #100.
        self.errors = {}

    def _get_form(self, data):
        # We disable _get_form() of UnicornView as we do our own form handling.
        return None
