import os

from setuptools import find_namespace_packages, setup

from wa_backstage import __version__

__dir__ = os.path.abspath(os.path.dirname(__file__))

try:
    with open(os.path.join(__dir__, "README.md")) as f:
        long_description = "\n" + f.read()
except FileNotFoundError:
    long_description = ""

setup(
    name="wa-backstage",
    version=__version__,
    description="Admin backend for Workadventure",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/wa-admin-panel",
    author="wa-backstage developers",
    author_email="mail@wa-backstage.org",
    license="AGPL-3.0",
    packages=find_namespace_packages(include=("wa_backstage", "wa_backstage.*")),
    install_requires=[
        "django>=3.2",
    ],
    include_package_data=True,
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Framework :: Django :: 3.2",
        "License :: OSI Approved :: GNU Affero General Public License v3",
        "Programming Language :: Python",
        "Programming Language :: Python :: 3 :: Only",
        "Topic :: Internet :: WWW/HTTP",
    ],
)
