# Release Documentation

- [ ] Copy this checklist to an issue and handle it there.
- [ ] Make sure we are not releasing (hardcoded) sensitive (credentials), unnecessary files like e.g. internal server addresses
- [ ] Run ``handsdown`` documentation generator and check in the generated files
- [ ] Fill in _CHANGELOG.md_ according to proposed [keepachangelog conventions](https://keepachangelog.com/en/1.0.0/)
- [ ] _Freeze_ develop branch state and tag the HEAD with a [semantic version number](https://semver.org/)
- [ ] Push develop branch (with tags) to _a new release branch_ and write a reasonable commit message (based on CHANGELOG.md)
- [ ] Have team members check out this _release branch_, test it and approve it
- [ ] Merge release branch into **main branch**
