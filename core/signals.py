# python imports
from slugify import slugify

# django imports
from django import utils
from django.dispatch.dispatcher import receiver
from django.db.models.signals import pre_save, post_save, m2m_changed
from django.contrib.auth.tokens import default_token_generator
from django.core.mail import send_mail

from django.template.loader import render_to_string
from wa_backstage import settings

# project imports
from core.models import CharacterTexture, MembershipWorld, Player, Room, World, BackstageUser


@receiver(post_save, sender=BackstageUser)
def send_backstage_user_activation_mail(sender, instance, created, **kwargs):
    """
    Send an email for password setting to a newly created backstage user.

    """
    if created:
        uid = utils.http.urlsafe_base64_encode(utils.encoding.force_bytes(instance.id))
        token = default_token_generator.make_token(instance)
        message = render_to_string(
            "core/password_set_email.html",
            {
                "user": instance,
                "site_name": "WA-Backstage",
                "protocol": "http",
                "domain": settings.SITE_URL,
                "uid": uid,
                "token": token,
            },
        )
        send_mail(
            subject="Welcome to WA-Backstage",
            message=message,
            from_email=settings.DEFAULT_FROM_EMAIL,
            recipient_list=[instance.email],
        )


@receiver(pre_save, sender=World)
def set_world_slug(sender, instance, update_fields=None, **kwargs):
    """
    receiver to auto set slug on world
    """
    instance.slug = slugify(instance.name)


@receiver(post_save, sender=World)
def create_anonymous_membership(sender, instance, created, **kwargs):
    """
    together with each new world we create the membership of an anonymous user
    """
    if created:
        player, _ = Player.objects.get_or_create(username="anonymous", email="invalid@email.local")
        membership = MembershipWorld(player=player, world=instance, anonymous=True)
        membership.save()


@receiver(post_save, sender=Room)
def only_one_entry_room(sender, instance, **kwargs):
    """
    if room is entry_room unset all other entry_rooms
    """
    if instance.is_entry_room:
        Room.objects.filter(world=instance.world, is_entry_room=True).exclude(pk=instance.pk).update(
            is_entry_room=False
        )


# business logic for character_textures auto assignment
@receiver(m2m_changed, sender=CharacterTexture.tags.through)
@receiver(m2m_changed, sender=CharacterTexture.world_set.through)
@receiver(post_save, sender=CharacterTexture)
def assign_matching_character_textures_to_members(sender, instance, **kwargs):
    """
    CTs are assigned to members with a logic of policy_type and tags
    We save and update these assignments in the db for performance reasons
    When a CT is created/updated, it is automatically assigned/reassigned to members
    """
    if instance.is_deactivated:
        instance.membershipworld_set.clear()
        return
    if instance.policy_type == 1:
        # add CTs to all members of all associated worlds
        instance.membershipworld_set.clear()
        members = MembershipWorld.objects.filter(world__in=instance.world_set.all())
        instance.membershipworld_set.add(*members)
    elif instance.policy_type == 2:
        # add CTs to all members of all associated worlds but not anon users
        instance.membershipworld_set.clear()
        members = MembershipWorld.objects.filter(world__in=instance.world_set.all(), anonymous=False)
        instance.membershipworld_set.add(*members)
    elif instance.policy_type == 3:
        # add only to members with matching tags
        instance.membershipworld_set.clear()
        members = MembershipWorld.objects.filter(
            world__in=instance.world_set.all(), anonymous=False, tags__in=instance.tags.all()
        )
        instance.membershipworld_set.add(*members)


@receiver(m2m_changed, sender=MembershipWorld.tags.through)
def assign_matching_character_textures_to_member(sender, instance, **kwargs):
    instance.character_textures.clear()
    character_textures = CharacterTexture.objects.filter(
        policy_type=3,
        tags__in=instance.tags.all(),
        world__in=[instance.world],
        is_deactivated=False,
    ) | CharacterTexture.objects.filter(policy_type__in=[1, 2], world__in=[instance.world], is_deactivated=False)
    instance.character_textures.add(*character_textures)
